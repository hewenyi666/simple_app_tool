from tkinter import *
import tkinter.font as tkFont
import os
import time
from datetime import datetime
from pathlib import Path
import threading
import sys
import shutil

root = Tk()
root.title("simple app tool")
root.geometry('520x600')  # 设置窗口大小

# 设置字体
restart_font = tkFont.Font(family="Times", size=20, weight="bold", slant="roman", underline=0)  # 创建字体对

common_font = tkFont.Font(family="Times", size=12, weight="normal", slant="roman", underline=0)  # 创建字体对

# current_path = os.path.dirname(os.path.abspath(__file__))  # 当前程序所在的目录  不能使用这个，否则打包出来exe的工作路径有问题
current_path = "."  # 当前程序所在的目录

try:
    # 检查log目录是否存在, 没有就创建目录
    if not Path(current_path + "/log").exists():
        os.mkdir("log")
except:
    pass

phone_dir = ""  # demo apk在手机的运行目录
phone_type = ""  # 连接的手机类型

phone_list = {"abcdefgh": "xiaomi", "ijklmnop": "Redmi"}  # abcdefgh 和 ijklmnop 应为真实手机的device_id!

text = Text(root, width=40, height=25)
text.grid(row=9, column=1)
text.insert(1.0, "demo的工作路径:\n\n")
text.insert("insert", "/storage/emulated/legacy/demo_test\n\n")  # 打印apk在手机的工作目录


def check_device() -> bool:
    res_device = os.popen("adb devices").readlines()
    try:
        current_device_id = res_device[1].split()[0]
    except IndexError:
        return False

    global phone_type
    global phone_dir
    phone_type = phone_list.get(current_device_id)

    if phone_type is None:  # 连接的设备不在预期的phone_list时
        phone_type = "未知设备"
        phone_dir = "/storage/emulated/legacy/demo_test"  # 手机是未知设备时,选择的工作目录
        return True

    if phone_type == "xiaomi":  # 根据识别的手机型号, 选择特定的工作目录
        phone_dir = "/storage/emulated/legacy/demo_test"
    elif phone_type == "Redmi":
        phone_dir = "/storage/self/primary/demo_test"
    return len(res_device) == 3


if check_device():
    connect_msg = "连接的设备:  " + phone_type
else:
    connect_msg = "当前无已连接的设备!!!"
    with open("error.log", "w") as f:
        f.write(connect_msg)
    print(connect_msg)
    sys.exit(0)

theLabel = Label(root, text=connect_msg, pady=8, font=common_font)
theLabel.grid(row=0, column=1)


def get_phone_ip():  # 设备ip
    try:
        adb_ip = os.popen("adb shell ifconfig wlan0").read().split(" ")[2]
    except IndexError:
        adb_ip = "因权限问题,未获取到手机的真实ip"
    return adb_ip


theLabel = Label(root, text="设备ip: " + get_phone_ip(), font=common_font)
theLabel.grid(row=1, column=1)


def get_cpu_type():  # 设备位数
    cpu_type = os.popen("adb shell getprop ro.product.cpu.abi").readlines()[0].strip()
    return cpu_type


theLabel = Label(root, text="cpu内核: " + get_cpu_type(), pady=8, font=common_font)
theLabel.grid(row=2, column=1)


def export_ini_phone():  # 导出手机配置
    export_ini_cmd = "adb pull " + phone_dir + "/config/demo.ini " + current_path  # 应为手机中文件的路径
    os.system(export_ini_cmd)
    text.insert("insert", "已导出demo.ini\n\n")


def import_ini_phone():  # 导入配置到手机
    # 检测本地配置文件是否存在
    if not Path(current_path + "/complete_ini/demo.ini").is_file():
        print("当前的 complete_ini 目录中不存在 demo.ini")
        return False
    phone_ini_path = phone_dir + "/config"
    export_ini_cmd = "adb push " + current_path + "/complete_ini/demo.ini " + " " + phone_ini_path
    os.system(export_ini_cmd)
    text.insert("insert", "已将demo.ini导入至手机\n\n")


def export_funshion_log():  # 导出手机的日志
    phone_log_path = phone_dir + "/log/demo.log"

    export_log_cmd = "adb pull " + phone_log_path + " " + current_path + "/log"  # 导出手机中日志到当前目录
    os.popen(export_log_cmd)
    text.insert("insert", "demo.log已导出至log文件夹\n\n")


def start_logcat():  # logcat记录日志
    current_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    log_name = current_path + "/log/" + current_time + ".log"
    print(log_name)
    logcat_cmd = "adb logcat -v time > " + log_name
    os.popen(logcat_cmd)
    thread_stop_logcat = threading.Thread(target=auto_stop_logcat)  # 耗时操作, 需要开启一个线程，否则界面会无响应
    thread_stop_logcat.start()
    text.insert("insert", "已开启logcat(默认300s后自动关闭)\n\n")


def auto_stop_logcat():  # 追加容错机制, logcat忘记手动点击关闭时的情况
    time.sleep(300)  # 无操作300秒后会自动结束记录日志
    stop_logcat()


def stop_logcat():  # 结束logcat
    search_logcat_cmd = "adb shell ps | findstr logcat"
    logcat_pid = os.popen(search_logcat_cmd).readlines()[0].split()[1]
    stop_logcat_cmd = "adb shell kill " + logcat_pid
    os.popen(stop_logcat_cmd)
    text.insert("insert", "已结束logcat\n\n")


def restart_app():  # 重启app
    # 获取当前界面应用信息的adb命令 adb shell dumpsys window | findstr mCurrentFocus
    os.popen("adb shell pm clear com.ss.android.article.news")  # 应为app的包名
    time.sleep(1)  # 必要的等待，apk需要一定时间启动
    os.popen("adb shell am start -n com.ss.android.article.news.activity.MainActivity")  # app的activity信息
    text.insert("insert", "AndroidProxyTest已重启\n\n")


def clear_text():
    text.delete(1.0, "end")


def delete_all_log():
    shutil.rmtree("log")
    os.mkdir("log")
    text.insert("insert", "已删除log文件夹中所有的文件\n\n")


Button(root, text="重启应用", font=restart_font, command=restart_app).grid(row=5, column=1)
Button(root, text="导出手机配置", command=export_ini_phone, font=common_font).grid(row=4, column=0, pady=6)
Button(root, text="导出手机log", command=export_funshion_log, font=common_font).grid(row=5, column=0, pady=6)

Button(root, text="导入手机配置", command=import_ini_phone, font=common_font).grid(row=6, column=0, pady=6)
Button(root, text="开启logcat", command=start_logcat, font=common_font).grid(row=4, column=2, pady=6)
Button(root, text="结束logcat", command=stop_logcat, font=common_font).grid(row=5, column=2, pady=6)
Button(root, text="删除所有日志", command=delete_all_log, font=common_font).grid(row=6, column=2, pady=6)
Button(root, text="清空日志", command=clear_text, font=common_font).grid(row=7, column=1, pady=5)

# 进入消息循环
root.mainloop()
